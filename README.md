# INFRA Location Extractor
Extracts INFRA (Independent Natural Food Retailers Association) locations from the official website, geocodes them, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Simplekml for easily building KML files
    * JSON module for JSON-based geodata
    * Regular expressions module for cleaning up messier-than-normal text
* Also of course depends on official [INFRA](https://www.naturalfoodretailers.net/member-directory) website.
