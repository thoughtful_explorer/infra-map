#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml
import json
import re

#Set filename/path for KML file output
kmlfile = "infra.kml"
#Set KML schema name
kmlschemaname = "infa"
#Set page URL
pageURL = "https://www.naturalfoodretailers.net/member-directory"
#Start a session with the given page URL
session = requests.Session()
page = session.get(pageURL)
#Soupify the HTML
soup = BeautifulSoup(page.content, 'html.parser')
#Find the 14th instance of the <script> tag - that one contains all the geo points
script = soup.select("script:nth-of-type(15)")
#Convert the contents of the <script> tag to a string, remove the first 62 characters and last 18 characters used in the HTML and convert into true JSON object
jsonpoints = json.loads(str(script[0].contents)[62:-18])
#Filter the JSON down to just the features list
jsonpoints = jsonpoints["openlayers"]["maps"]["openlayers-map"]["layers"]["members_view_page_openlayers_data_overlay"]["features"]

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Create dictionary of weird JSON unicode values to make the store names appear as they should
dictionary = {"\\u0026amp;":"&","\\u0026#039;":"'","\\u00f1":"ñ","\\u00e9":"é","\/":"/","\\u2013":"+","\\u2019":"'","\\u2014":" - "}

#Iterate through all the JSON points used by leaflet
for point in jsonpoints:
    #Part 1/2: split off the storename from the messy unicode using the first instance of messy unicode as the delimiter
    storename = re.split("\\\\u0022\\\\u003E", point["attributes"]["title"])[3]
    #Part 2/2: split off the store name from the beginning of what we got in part 1 and ignoring the rest 
    storename = re.split("\\\\u003C", storename)[0]
    #Iterate through the unicode dictionary and visually repair store names that contained encoded unicode
    for key in dictionary.keys():
        storename = storename.replace(key, dictionary[key])
    #Skip this store if there are no coordinates provided with it
    if point["wkt"] is None:
        continue
    #The coordinates are easy to find and in a relatively easy format; remove the first 7 characters, then remove the surrounding parenthesis, and split in two by " "
    latlng = re.split(" ",point["wkt"][6:].strip('()'))
    lat = latlng[1]
    lng = latlng[0]
    #Extract the address data from the field_address field using the first (and only) unique result of each findall and converting it into a string
    street = str(re.findall("thoroughfare\\\\u0022\\\\u003E(.*?)\\\\u003C",point["attributes"]["field_address"])[0])
    locality = str(re.findall("locality\\\\u0022\\\\u003E(.*?)\\\\u003C",point["attributes"]["field_address"])[0])
    state = str(re.findall("state\\\\u0022\\\\u003E(.*?)\\\\u003C",point["attributes"]["field_address"])[0])
    postalcode = str(re.findall("postal-code\\\\u0022\\\\u003E(.*?)\\\\u003C",point["attributes"]["field_address"])[0])
    #Rebuild a human readable address with street, locality, state, postalcode
    storeaddress = street + " " + locality + ", " + state + " " + postalcode
    #Create the point name and description in the kml
    point = kml.newpoint(name=storename,description="market")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)

